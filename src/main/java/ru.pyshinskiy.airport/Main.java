package ru.pyshinskiy.airport;

import com.skillbox.airport.Airport;

public class Main {

    public static void main(String[] args) {
        Airport.getInstance().getAllAircrafts().forEach(System.out::println);
    }
}